package com.sysomos.neuralnet.inference.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;

import au.com.bytecode.opencsv.CSVWriter;

public class FileUtils {

	public static void write(String filepath, List<String[]> lines)
			throws IOException {
		if (CollectionUtils.isEmpty(lines))
			return;
		CSVWriter writer = new CSVWriter(new FileWriter(filepath), ',');
		writer.writeAll(lines);
		writer.close();
	}

	public static void save(String filepath, String content)
			throws IOException {
		List<String[]> lines = new ArrayList<String[]>();
		lines.add(new String[] { content });
		write(filepath, lines);
	}

	public static void writeProperties(String filepath,
			Map<String, String> properties) throws IOException {
		if (MapUtils.isEmpty(properties))
			return;
		List<String[]> lines = new ArrayList<String[]>();
		for (Map.Entry<String, String> e : properties.entrySet()) {
			lines.add(new String[] { e.getKey() + "=" + e.getValue() });
		}
		write("model.properties", lines);
	}

	public static void write(List<DataSet> datasets) throws IOException {
		if (CollectionUtils.isEmpty(datasets))
			return;
		List<String[]> lines = new ArrayList<String[]>();
		for (DataSet instance : datasets) {
			if (instance == null)
				continue;
			INDArray values = instance.getFeatures();
			String str[] = new String[values.columns() + 1];
			for (int i = 0; i < str.length - 1; i++) {
				str[i] = String.valueOf(values.getDouble(i));
			}

			INDArray labels = instance.getLabels();
			for (int i = 0; i < labels.columns(); i++) {
				if ((int) labels.getDouble(i) == 1) {
					str[str.length - 1] = String.valueOf(i);
					break;
				}
			}
			lines.add(str);
		}
		write("model.dat", lines);
	}

}

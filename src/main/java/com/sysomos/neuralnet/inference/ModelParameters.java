package com.sysomos.neuralnet.inference;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.sysomos.text.sentenceiterator.FileSentenceIterator;
import com.sysomos.text.sentenceiterator.SentenceIterator;
import com.sysomos.text.sentenceiterator.TweetItemIterator;
import com.sysomos.word2vec.loader.WordVectorSerializer;
import com.sysomos.word2vec.word.vectors.WordVectors;

public class ModelParameters {
	@Parameter(names = { "-momentum",
			"-a" }, required = false, description = "", validateWith = BoundedValue.class)
	public double momentum = 0.9;
	@Parameter(names = { "-binet",
			"-b" }, required = true, description = "Location of the bigram neural net. serialized "
					+ "model data", converter = Word2VecLoader.class)
	public WordVectors wordVectors;
	@Parameter(names = { "-duration",
			"-d" }, required = false, description = "Size of the training window to consider when "
					+ "training the neural net model", validateWith = PositiveInteger.class)
	public int duration = 30;
	@Parameter(names = { "-feeds",
			"-f" }, required = false, description = "Location of Twitter feeds to process to build "
					+ "the neural network model.")
	public String hdfsUrl = "hdfs://labdevhdfscluster/twitter/TTFHRawPost/";
	@Parameter(names = { "-hLayer",
			"-h" }, required = false, validateWith = PositiveInteger.class, description = "Number "
					+ "of hidden layer(s)")
	public int hiddenLayers = 4;
	@Parameter(names = { "-iterations",
			"-i" }, required = false, description = "The number of times the net classifies "
					+ "samples and corrects them with a weight "
					+ "update", validateWith = PositiveInteger.class)
	public int iterations = 1000;
	@Parameter(names = { "-hUnit",
			"-k" }, required = false, validateWith = PositiveInteger.class, description = "Number "
					+ "of hidden units to consider in a hidden layer")
	public int hiddenUnits = 70;
	@Parameter(names = { "-numHoldout",
			"-n" }, required = true, description = "Percentage of the size of the dataset to hold "
					+ "out for training", validateWith = BoundedValue.class)
	public float holdout;
	@Parameter(names = { "-objective",
			"-o" }, required = true, description = "Model objective", validateWith = NonEmptyString.class)
	public String objective;
	@Parameter(names = { "-regularization",
			"-r" }, required = false, description = "Regularization term to fight overfitting when "
					+ "training model", validateWith = BoundedValue.class)
	public double regularizationTerm = 2e-4;
	@Parameter(names = { "-lRate",
			"-s" }, required = false, validateWith = BoundedValue.class, description = "Learning "
					+ "rate for stochastic gradient descent")
	public double learningRate = 1e-3;
	@Parameter(names = { "-infile",
			"-t" }, required = true, description = "Seed file to use to create training instances "
					+ "to build the neural net model", converter = TrainingDataConverter.class)
	public TrainingData training;
	@Parameter(names = { "-usePreviousRun",
			"-u" }, required = false, description = "Re-use previous model input data to train new "
					+ "model")
	public boolean usePreviousRun = false;
	@Parameter(names = { "-wordFreq",
			"-w" }, required = false, description = "TF-IDF min value to consider for vocab words "
					+ "", validateWith = PositiveInteger.class)
	public int minWordFreq = 10;

	public static class BoundedValue implements IParameterValidator {
		@Override
		public void validate(String name, String value) {
			float val = Float.valueOf(value);
			if (val < 0 || val > 1) {
				throw new ParameterException("Parameter " + name
						+ " should be in [0, 1] (found " + value + ")");
			}
		}
	}

	public static class PositiveInteger implements IParameterValidator {

		@Override
		public void validate(String name, String value)
				throws ParameterException {
			int n = Integer.parseInt(value);
			if (n <= 0) {
				throw new ParameterException("Parameter " + name
						+ " should be positive (found " + value + ")");
			}
		}
	}

	public static class SentenceIteratorConverter
			implements IStringConverter<SentenceIterator> {

		@Override
		public SentenceIterator convert(String path) {
			TweetItemIterator iter = new TweetItemIterator(new File(path));
			iter.setIgnoreRetweets(true);
			return iter;
		}
	}

	public static class Word2VecLoader
			implements IStringConverter<WordVectors> {

		@Override
		public WordVectors convert(String path) {
			File file = new File(path);
			try {
				return WordVectorSerializer.loadTxtVectors(file);
			} catch (IOException e) {
				return null;
			}
		}
	}

	public static class TrainingDataConverter
			implements IStringConverter<TrainingData> {

		private static final Logger LOG = LoggerFactory
				.getLogger(TrainingDataConverter.class);

		@Override
		public TrainingData convert(String filename) {
			if (StringUtils.isEmpty(filename))
				return null;
			try {
				return new TrainingData(filename);
			} catch (IOException e) {
				LOG.error(e.getLocalizedMessage(), e);
			}
			return null;
		}
	}

	public static class NonEmptyString implements IParameterValidator {

		@Override
		public void validate(String name, String value)
				throws ParameterException {
			if (StringUtils.isEmpty(value))
				throw new ParameterException(
						"Expecting a valid name (name is empty or null)");
		}

	}

	public static final class TrainingData {
		public Map<Long, Integer> instances;
		public int numClassLabels = 0;

		public TrainingData(String filename) throws IOException {

			instances = new HashMap<Long, Integer>();
			Set<Integer> classes = new HashSet<Integer>();
			File file = new File(filename);
			FileSentenceIterator iterator = new FileSentenceIterator(file);
			while (iterator.hasNext()) {
				String line = iterator.nextSentence();
				String[] tokens = line.split(",");
				if (tokens == null || tokens.length < 2)
					continue;
				Long id = Long.valueOf(tokens[0]);
				if (id == null)
					continue;
				classes.add(Integer.valueOf(tokens[1]));
				instances.put(id, Integer.valueOf(tokens[1]));
			}
			numClassLabels = classes.size();
		}
	}
}

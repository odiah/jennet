package com.sysomos.neuralnet.inference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.RBM;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.params.DefaultParamInitializer;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.api.IterationListener;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.SplitTestAndTrain;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.sysomos.base.filter.rule.BaseRule;
import com.sysomos.neuralnet.inference.data.InferenceDataFetcher;
import com.sysomos.neuralnet.inference.data.InferenceDataSetIterator;
import com.sysomos.neuralnet.inference.exception.DataFetcherException;
import com.sysomos.neuralnet.inference.exception.ModelBuildingException;
import com.sysomos.neuralnet.inference.utils.FileUtils;
import com.sysomos.text.filter.RulePipeline;
import com.sysomos.text.filter.TweetItemFilterFactory;
import com.sysomos.text.filter.rule.IgnoreIfCreatedAtNotInRule;
import com.sysomos.text.filter.rule.IgnoreIfEmptyRule;
import com.sysomos.text.filter.rule.IgnoreIfIdsNotInRule;
import com.sysomos.text.filter.rule.IgnoreIfIsMentionRule;
import com.sysomos.text.filter.rule.IgnoreIfIsRetweetRule;
import com.sysomos.text.filter.rule.IgnoreIfLengthLessThanRule;
import com.sysomos.text.sentenceiterator.CollectionSentenceIterator;
import com.sysomos.text.sentenceiterator.HadoopTweetItemIterator;
import com.sysomos.text.sentenceiterator.TweetItem;
import com.sysomos.text.tokenizer.factory.DefaultTokenizerFactory;
import com.sysomos.text.utils.StopWords;
import com.sysomos.word2vec.VocabWord;
import com.sysomos.word2vec.word.vectors.WordVectors;
import com.sysomos.word2vec.words.vectorizer.TextVectorizer;
import com.sysomos.word2vec.words.vectorizer.TfidfVectorizer;
import com.sysomos.word2vec.wordstore.VocabCache;

public class ModelBuilder {

	private static final String MODEL_CONFIG = "model-conf.dat";
	private static final String MODEL_WEIGHTS = "model-weights.dat";
	private static final int seed = 123;
	private static final Logger LOG = LoggerFactory
			.getLogger(ModelBuilder.class);

	private static RulePipeline pipeline;
	private static List<Class<? extends BaseRule>> rules;

	static {
		rules = new ArrayList<Class<? extends BaseRule>>();

		rules.add(IgnoreIfEmptyRule.class);
		rules.add(IgnoreIfIsRetweetRule.class);
		rules.add(IgnoreIfIsMentionRule.class);
		rules.add(IgnoreIfLengthLessThanRule.class);

		pipeline = new RulePipeline(rules);
	}

	/**
	 * This is the main method to call to build the inference model. Depending
	 * on the parameters that are set, this model can either be a feed-forward
	 * neural network with one hidden layer or a deep learning model.
	 * 
	 * @param params
	 *            defines all possible parameters of the model (@see
	 *            {@link ModelParameters})
	 * @throws ModelBuildingException
	 *             thrown when training data is missing or the bigram neural
	 *             network model serialized data cannot be accessed.
	 */
	public void build(final ModelParameters params)
			throws ModelBuildingException {

		LOG.info("Loading and normalizing " + params.objective + "  data ....");

		DataSet dataset = generate(params);
		if (dataset == null || dataset.getFeatureMatrix() == null)
			throw new ModelBuildingException("Cannot load data for inference");
		dataset.normalizeZeroMeanZeroUnitVariance();

		LOG.info("Generating training and testing data ....");

		int numInstances = dataset.getFeatureMatrix().rows();
		int training = (int) (numInstances * params.holdout);

		SplitTestAndTrain testAndTrain = dataset.splitTestAndTrain(training);
		DataSet trainingSet = testAndTrain.getTrain();
		DataSet testingSet = testAndTrain.getTest();

		LOG.info(params.objective + " Inference Model [ training: " + training
				+ ", testing: " + (numInstances - training) + " ] ....");

		int numFeatures = trainingSet.get(0).numInputs();

		MultiLayerConfiguration conf = configure(numFeatures,
				params.training.numClassLabels, params.momentum,
				params.learningRate, params.regularizationTerm,
				params.hiddenLayers, params.hiddenUnits, params.iterations);

		LOG.info(params.objective + " neural net model training ....");

		MultiLayerNetwork model = new MultiLayerNetwork(conf);
		model.init();

		// ScoreIterationListener is a hook that monitors the iterations and
		// reacts to what’s happening like printing errors, plotting weight
		// distribution, or the values output by the activation functions.
		model.setListeners(Collections
				.singletonList((IterationListener) new ScoreIterationListener(
						params.iterations / 5)));
		model.fit(trainingSet);
		persist(model);

		LOG.info(params.objective + " neural net model evaluation ....");

		INDArray output = model.output(testingSet.getFeatureMatrix());

		List<String[]> lines = new ArrayList<String[]>();
		lines.add(new String[] { "actual", "predicted" });
		for (int i = 0; i < output.rows(); i++) {
			String actual = testingSet.getLabels().getRow(i).toString().trim();
			String predicted = output.getRow(i).toString().trim();
			lines.add(new String[] { actual, predicted });
		}
		try {
			String file = params.objective + "Eval.csv";
			FileUtils.write(file, lines);
			LOG.info("Evaluation results saved in "
					+ System.getProperty("user.dir") + "/" + file);
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}

		Evaluation eval = new Evaluation(params.training.numClassLabels);
		eval.eval(testingSet.getLabels(), output);
		LOG.info(params.objective + " Statistics ....\n" + eval.stats());
	}

	private MultiLayerConfiguration configure(int numFeatures,
			int numClassLabels, double momentum, double learningRate,
			double regularizationTerm, int hiddenLayers, int hiddenUnits,
			int iterations) {
		MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
				// Locks in weight initialization for tuning
				.seed(seed)
				// # training iterations predict/classify & backprop
				.iterations(iterations)
				// Optimization step size
				.learningRate(learningRate)
				// Backprop to calculate gradients
				.optimizationAlgo(OptimizationAlgorithm.CONJUGATE_GRADIENT)
				.l1(1e-1).regularization(true).l2(2e-4).useDropConnect(true)
				// # NN layers (doesn't count input layer)
				.list(hiddenLayers)
				.layer(0,
						new RBM.Builder(RBM.HiddenUnit.RECTIFIED,
								RBM.VisibleUnit.GAUSSIAN)
										// number of input nodes
										.nIn(numFeatures)
										// # fully connected hidden layer nodes.
										// Add list if multiple layers.
										.nOut(hiddenUnits)
										// Weight initialization
										.weightInit(WeightInit.XAVIER).k(
												1) // # contrastive divergence
													// iterations
								.activation("relu") // Activation function type
								.lossFunction(
										LossFunctions.LossFunction.RMSE_XENT) // Loss
																				// function
																				// type
								.updater(Updater.ADAGRAD).dropOut(0.5).build())
				.layer(1,
						new RBM.Builder(RBM.HiddenUnit.RECTIFIED,
								RBM.VisibleUnit.GAUSSIAN).nIn(hiddenUnits)
										.nOut(hiddenUnits)
										.weightInit(WeightInit.XAVIER).k(1)
										.activation("tanh")
										.lossFunction(
												LossFunctions.LossFunction.RMSE_XENT)
								.updater(Updater.ADAGRAD).dropOut(0.5).build())
				.layer(2,
						new RBM.Builder(RBM.HiddenUnit.RECTIFIED,
								RBM.VisibleUnit.GAUSSIAN).nIn(hiddenUnits)
										.nOut(numClassLabels)
										.weightInit(WeightInit.XAVIER).k(1)
										.activation("tanh")
										.lossFunction(
												LossFunctions.LossFunction.RMSE_XENT)
								.updater(Updater.ADAGRAD).dropOut(0.5).build())
				.layer(3,
						new OutputLayer.Builder(
								LossFunctions.LossFunction.MCXENT) // 3
										.nIn(numClassLabels) // # input nodes
										.nOut(numClassLabels) // # output nodes
										.activation("softmax").build())
				.backprop(true).pretrain(true).build();

		return conf;
	}

	private DataSet generate(final ModelParameters params)
			throws ModelBuildingException {
		if (params == null || params.wordVectors == null)
			throw new ModelBuildingException(
					"One or more required parameters are null");
		if (params.usePreviousRun) {
			try {
				InferenceDataFetcher fetcher = new InferenceDataFetcher();
				fetcher.fetch(fetcher.totalExamples());
				DataSet dataset = fetcher.next();
				if (!(dataset == null || dataset.getFeatureMatrix() == null))
					return dataset;
			} catch (DataFetcherException e) {
				LOG.warn(e.getLocalizedMessage(), e);
			}
		}

		pipeline.ruleInstances.add(
				new IgnoreIfIdsNotInRule(params.training.instances.keySet()));
		pipeline.ruleInstances
				.add(new IgnoreIfCreatedAtNotInRule(params.duration));

		// pipeline.ruleInstances.add(new
		// IgnoreIfLanguageNotInRule(Arrays.asList(
		// new String[] { "en", "fr", "ar", "es", "pt", "et", "fa", "it" })));

		HadoopTweetItemIterator tweetIter = new HadoopTweetItemIterator(
				params.hdfsUrl, params.duration,
				TweetItemFilterFactory.create(pipeline));

		Multimap<Long, String> posts = ArrayListMultimap.create();
		while (tweetIter.hasNext()) {
			TweetItem tweet = tweetIter.nextTweet();
			if (tweet != null)
				posts.put(tweet.user.id, tweet.text);
		}

		List<DataSet> datasets = new ArrayList<DataSet>();
		for (long userId : posts.keySet()) {
			TextVectorizer vectorizer = new TfidfVectorizer.Builder()
					.iterate(new CollectionSentenceIterator(posts.get(userId)))
					.minWords(params.minWordFreq)
					.stopWords(StopWords.getStopWords())
					.tokenize(new DefaultTokenizerFactory()).build();
			vectorizer.fit();

			INDArray projection = project(vectorizer.vocab(),
					params.wordVectors);
			if (projection == null)
				continue;
			double[] rowOutcome = new double[params.training.numClassLabels];
			rowOutcome[params.training.instances.get(userId)] = 1;
			datasets.add(new DataSet(projection, Nd4j.create(rowOutcome)));
		}
		if (CollectionUtils.isEmpty(datasets))
			throw new ModelBuildingException(
					"Cannot train model with null or empty training dataset.");

		DataSetIterator iter = new InferenceDataSetIterator(datasets.size(),
				datasets.size(),
				new InferenceDataFetcher(datasets.get(0).numInputs(),
						params.training.numClassLabels, datasets));
		return iter.next();
	}

	private INDArray project(VocabCache cache, WordVectors wordVectors) {
		if (cache == null || CollectionUtils.isEmpty(cache.tokens())
				|| wordVectors == null)
			return null;
		List<VocabWord> list = new ArrayList<VocabWord>(cache.tokens());
		INDArray matrix = null;
		int i = 0;
		do {
			String word = list.get(i).getWord();
			matrix = wordVectors.getWordVectorMatrix(word);
			if (matrix != null) {
				i++;
				break;
			}
		} while (++i < list.size());
		int total = 1;
		for (; i < list.size(); i++) {
			String word = list.get(i).getWord();
			if (wordVectors.getWordVectorMatrix(word) == null)
				continue;
			INDArray matr = wordVectors.getWordVectorMatrix(word);
			if (matr == null)
				continue;
			int freq = cache.wordFrequency(word);
			total += freq;
			matrix.add(matr.mul(freq));
		}
		return matrix == null ? matrix : matrix.div(total);
	}

	private void persist(MultiLayerNetwork model) {
		if (model == null || model.getLayerWiseConfigurations() == null)
			return;
		List<String[]> weights = new ArrayList<String[]>();
		for (org.deeplearning4j.nn.api.Layer layer : model.getLayers()) {
			INDArray w = layer.getParam(DefaultParamInitializer.WEIGHT_KEY);
			String[] values = new String[w.columns()];
			for (int i = 0; i < w.columns(); i++) {
				values[i] = String.valueOf(w.getColumn(i));
			}
			weights.add(values);
		}

		try {
			FileUtils.save(MODEL_CONFIG,
					model.getLayerWiseConfigurations().toJson());
			FileUtils.write(MODEL_WEIGHTS, weights);
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	public static void main(String args[]) {
		ModelParameters params = new ModelParameters();
		new JCommander(params, args).parse();
		ModelBuilder builder = new ModelBuilder();
		try {
			builder.build(params);
		} catch (ModelBuildingException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}
}

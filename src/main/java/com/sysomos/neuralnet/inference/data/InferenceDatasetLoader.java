package com.sysomos.neuralnet.inference.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.springframework.core.io.ClassPathResource;

public class InferenceDatasetLoader {

	public static List<DataSet> load(int from, int to) throws IOException {
		ClassPathResource resource = new ClassPathResource("/model.dat");
		List<String> lines = IOUtils.readLines(resource.getInputStream());

		Properties prop = new Properties();
		prop.load(new ClassPathResource("/model.properties").getInputStream());

		int numOutcomes = Integer.parseInt(prop.getProperty("numOutcomes"));
		int inputColumns = Integer.parseInt(prop.getProperty("inputColumns"));

		INDArray ret = Nd4j.ones(Math.abs(to - from), inputColumns);
		double[][] outcomes = new double[lines.size()][numOutcomes];
		int putCount = 0;

		for (int i = from; i < to; i++) {
			String line = lines.get(i);
			String[] split = line.split(",");

			addRow(ret, putCount++, split);

			String outcome = split[split.length - 1];
			double[] rowOutcome = new double[numOutcomes];
			rowOutcome[Integer.parseInt(outcome)] = 1;
			outcomes[i] = rowOutcome;
		}

		List<DataSet> list = new ArrayList<DataSet>();
		for (int i = 0; i < ret.rows(); i++) {
			list.add(new DataSet(ret.getRow(i),
					Nd4j.create(outcomes[from + i])));
		}
		return list;
	}

	private static void addRow(INDArray ret, int row, String[] line) {
		double[] vector = new double[4];
		for (int i = 0; i < 4; i++)
			vector[i] = Double.parseDouble(line[i]);
		ret.putRow(row, Nd4j.create(vector));
	}

}

package com.sysomos.neuralnet.inference.data;

import org.nd4j.linalg.dataset.api.iterator.BaseDatasetIterator;
import org.nd4j.linalg.dataset.api.iterator.fetcher.DataSetFetcher;

public class InferenceDataSetIterator extends BaseDatasetIterator {

	private static final long serialVersionUID = -2919719390910186712L;

	public InferenceDataSetIterator(int batch, int numExamples,
			DataSetFetcher fetcher) {
		super(batch, numExamples, fetcher);
	}

}

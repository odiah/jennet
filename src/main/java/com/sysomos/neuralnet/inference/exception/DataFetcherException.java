package com.sysomos.neuralnet.inference.exception;

public class DataFetcherException extends Exception {

	private static final long serialVersionUID = 8055404681376881947L;

	public DataFetcherException(String message) {
		super(message);
	}

	public DataFetcherException(Throwable e) {
		super(e);
	}
}

package com.sysomos.neuralnet.inference.exception;

public class ModelBuildingException extends Exception {

	public ModelBuildingException(Throwable cause) {
		super(cause);
	}

	public ModelBuildingException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 5116723229010311113L;

}
